package salesman.flystudio.com.salesman.utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.HashMap;

/**
 * Created by Fly on 10.01.2015.
 */
public class Towns {

    public static void initTowns() {
        Dinamic.clear();
        Dinamic.addTown(new Town("berlin", new BasicNameValuePair("150", "200")));
        Dinamic.addTown(new Town("munich", new BasicNameValuePair("350", "250")));
        Dinamic.addTown(new Town("karlsruhe", new BasicNameValuePair("300", "200")));
        Dinamic.addTown(new Town("oberhausen", new BasicNameValuePair("350", "50")));
        Dinamic.addTown(new Town("hannover", new BasicNameValuePair("150", "150")));
        Dinamic.addTown(new Town("kassel", new BasicNameValuePair("250", "100")));
    }

    public static HashMap<String, Town> getTowns() {
        return Dinamic.getTowns();
    }



    static class Dinamic {
        static HashMap<String, Town> towns = new HashMap<>();

        public static HashMap<String, Town> getTowns() {
            return towns;
        }

        public static void clear() {
            towns.clear();
        }

        public static void addTown(Town town) {
            towns.put(town.getName(), town);
        }

        public static void removeTown(String name) {
            towns.remove(name);
        }
    }
}
