package salesman.flystudio.com.salesman.utils;

import android.content.Intent;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Fly on 10.01.2015.
 */
public class ShortestHamiltonianCycle {

    public static HashMap<Double, int[]> getShortestHamiltonianCycle(double[][] dist) {
        int n = dist.length;
        double[][] dp = new double[1 << n][n];
        for (double[] d : dp)
            Arrays.fill(d, Integer.MAX_VALUE / 2);
        dp[1][0] = 0;
        for (int mask = 1; mask < 1 << n; mask += 2) {
            for (int i = 1; i < n; i++) {
                if ((mask & 1 << i) != 0) {
                    for (int j = 0; j < n; j++) {
                        if ((mask & 1 << j) != 0) {
                            dp[mask][i] = Math.min(dp[mask][i], dp[mask ^ (1 << i)][j] + dist[j][i]);
                        }
                    }
                }
            }
        }
        double s = Integer.MAX_VALUE;
        for (int i = 1; i < n; i++) {
            s = Math.min(s, dp[(1 << n) - 1][i] + dist[i][0]);
        }
        int cur = (1 << n) - 1;
        int[] order = new int[n];
        int last = 0;
        for (int i = n - 1; i >= 1; i--) {
            int bj = -1;
            for (int j = 1; j < n; j++) {
                if ((cur & 1 << j) != 0 && (bj == -1 || dp[cur][bj] + dist[bj][last] > dp[cur][j] + dist[j][last])) {
                    bj = j;

                }
            }
            order[i] = bj;
            cur ^= 1 << bj;
            last = bj;
        }
        HashMap<Double, int[]> result = new HashMap<>();
        result.put(s, order);
        return result;
    }
}