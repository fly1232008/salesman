package salesman.flystudio.com.salesman.utils;

import org.apache.http.NameValuePair;

/**
 * Created by Fly on 10.01.2015.
 */
public class Town {
    String name;
    NameValuePair position;

    public Town(String name, NameValuePair position) {
        this.name = name;
        this.position = position;
    }

    public NameValuePair getPosition() {
        return position;
    }

    public void setPosition(NameValuePair position) {
        this.position = position;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
