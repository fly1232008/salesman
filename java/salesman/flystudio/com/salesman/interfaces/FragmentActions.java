package salesman.flystudio.com.salesman.interfaces;

/**
 * Created by Fly on 10.01.2015.
 */
public enum  FragmentActions {
    RESULT_OK, CANCEL, ERROR
}
