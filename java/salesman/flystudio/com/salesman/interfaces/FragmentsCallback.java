package salesman.flystudio.com.salesman.interfaces;

import android.content.Intent;
import android.support.v4.app.Fragment;

import salesman.flystudio.com.salesman.fragments.Fragments;

/**
 * Created by Fly on 10.01.2015.
 */
public interface FragmentsCallback {

    void callFromFragment(Fragment fragment, FragmentActions action, Intent ... intent);
    void callFromFragment(Fragment fragment, Fragments action);
}
