package salesman.flystudio.com.salesman;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import salesman.flystudio.com.salesman.fragments.Fragments;
import salesman.flystudio.com.salesman.interfaces.FragmentActions;
import salesman.flystudio.com.salesman.interfaces.FragmentsCallback;
import salesman.flystudio.com.salesman.utils.Town;
import salesman.flystudio.com.salesman.utils.Towns;


public class MainActivity extends ActionBarActivity implements FragmentsCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Config.init();
        showFragment(Fragments.MAIN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void callFromFragment(Fragment fragment, FragmentActions action, Intent... intent) {
    }

    @Override
    public void callFromFragment(Fragment fragment, Fragments action) {
        showFragment(action);
    }

    void showFragment(Fragments fragment, Intent ... intent) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (fragment) {
            case MAIN:
                ft.replace(R.id.container, fragment.getFragment(), fragment.toString());
                break;
            case CONTENT:
                ft.replace(R.id.container, fragment.getFragment(), fragment.toString());
                break;
            default:
                ft.replace(R.id.container, fragment.getFragment(), fragment.toString());
                break;
        }
        ft.commit();
    }
}
