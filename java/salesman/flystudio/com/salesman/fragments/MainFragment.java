package salesman.flystudio.com.salesman.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import salesman.flystudio.com.salesman.R;
import salesman.flystudio.com.salesman.interfaces.FragmentsCallback;

/**
 * Created by Fly on 10.01.2015.
 */
public class MainFragment extends Fragment {

    public MainFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        TextView button = (TextView) rootView.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FragmentsCallback)getActivity()).callFromFragment(MainFragment.this, Fragments.CONTENT);
            }
        });
        return rootView;
    }
}