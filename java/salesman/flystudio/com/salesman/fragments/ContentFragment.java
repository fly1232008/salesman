package salesman.flystudio.com.salesman.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import salesman.flystudio.com.salesman.R;
import salesman.flystudio.com.salesman.utils.ShortestHamiltonianCycle;
import salesman.flystudio.com.salesman.utils.Town;
import salesman.flystudio.com.salesman.utils.Towns;
import salesman.flystudio.com.salesman.view.MapView;

/**
 * Created by Fly on 10.01.2015.
 */
public class ContentFragment extends Fragment {
    TextView consol;
    double[][] matrix;
    Object[] townsName;
    Button start;
    MapView mapView;
    public ContentFragment() {
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_content, container, false);
        consol = (TextView) rootView.findViewById(R.id.consol);
        start = (Button) rootView.findViewById(R.id.startBtn);
        mapView = (MapView) rootView.findViewById(R.id.mapView);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                background();
            }
        });
        return rootView;
    }

    private void background() {
        getMatrix();
        consol.append("\n");
        HashMap<Double, int[]> route = ShortestHamiltonianCycle.getShortestHamiltonianCycle(matrix);
        ArrayList<Town> townsRoute = new ArrayList<Town>();
        for (Map.Entry<Double, int[]> el : route.entrySet()) {
            consol.append("длина пути: " + el.getKey());
            consol.append("\n маршрут: ");
            for (int i = 0; i < el.getValue().length; i++) {
                consol.append(townsName[el.getValue()[i]].toString() + ", ");
                townsRoute.add(Towns.getTowns().get(townsName[el.getValue()[i]].toString()));
            }
            consol.append("\n");
        }
        mapView.setRoute(townsRoute);
    }

    private void getMatrix() {
        HashMap<String, Town> towns = Towns.getTowns();
        townsName = towns.keySet().toArray();
        matrix = new double[towns.size()][towns.size()];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                Town t1 = towns.get(townsName[i]);
                Town t2 = towns.get(townsName[j]);
                matrix[i][j] =  Math.sqrt(((Integer.parseInt(t2.getPosition().getName()) - (Integer.parseInt(t1.getPosition().getName())))*(Integer.parseInt(t2.getPosition().getName()) - (Integer.parseInt(t1.getPosition().getName()))))
                        + ((Integer.parseInt(t2.getPosition().getValue()) - (Integer.parseInt(t1.getPosition().getValue())))*(Integer.parseInt(t2.getPosition().getValue()) - (Integer.parseInt(t1.getPosition().getValue())))));
            }
        }
    }


}