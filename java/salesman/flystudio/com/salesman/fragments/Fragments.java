package salesman.flystudio.com.salesman.fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;

/**
 * Created by Fly on 10.01.2015.
 */
public enum  Fragments {
    MAIN {
        @Override
        public Fragment getFragment(Intent... intent) {
            return new MainFragment();
        }
    },
    CONTENT {
        @Override
        public Fragment getFragment(Intent... intent) {
            return new ContentFragment();
        }
    };

    public abstract Fragment getFragment(Intent... intent);
}
