package salesman.flystudio.com.salesman.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import salesman.flystudio.com.salesman.utils.Town;
import salesman.flystudio.com.salesman.utils.Towns;

/**
 * Created by Fly on 10.01.2015.
 */
public class MapView extends View {

    Paint p;
    Paint lp;
    Paint tp;
    RectF rectf;
    float[] route;

    void init() {
        setBackgroundColor(Color.YELLOW);
        p = new Paint();
        lp = new Paint();
        tp = new Paint();
        rectf = new RectF(700,100,800,150);
        route = new float[]{};
        p.setColor(Color.RED);
        p.setStrokeWidth(10);
        tp.setColor(Color.RED);
        tp.setStrokeWidth(2);
        tp.setTextSize(20);
        lp.setColor(Color.GREEN);
        lp.setStrokeWidth(3);
    }

    public MapView(Context context) {
        super(context);
        init();
    }

    public MapView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setRoute(ArrayList<Town> list) {
        route = new float[(list.size()*4) -4];
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (i == 0 || i == list.size()-1) {
                route[count] = Float.parseFloat(list.get(i).getPosition().getName());
                count++;
                route[count] = Float.parseFloat(list.get(i).getPosition().getValue());
                count++;
            } else {
                route[count] = Float.parseFloat(list.get(i).getPosition().getName());
                count++;
                route[count] = Float.parseFloat(list.get(i).getPosition().getValue());
                count++;
                route[count] = Float.parseFloat(list.get(i).getPosition().getName());
                count++;
                route[count] = Float.parseFloat(list.get(i).getPosition().getValue());
                count++;
            }
        }
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        for (Map.Entry<String, Town> town : Towns.getTowns().entrySet()) {
            float x = Float.parseFloat(town.getValue().getPosition().getName());
            float y = Float.parseFloat(town.getValue().getPosition().getValue());
            canvas.drawPoint(x, y, p);
            canvas.drawText(town.getValue().getName(), x+20, y+20, tp);
        }

        if (route.length > 0)
            canvas.drawLines(route,lp);
    }
}
